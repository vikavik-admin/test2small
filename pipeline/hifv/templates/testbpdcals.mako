<%!
rsc_path = ""
import os
import pipeline.infrastructure.renderer.htmlrenderer as hr
%>
<%inherit file="t2-4m_details-base.mako"/>

<%block name="title">Initial test calibrations</%block>

<p>Initial test calibrations using bandpass and delay calibrators</p>

% for ms in summary_plots:
    <h4>Plots:  <a class="replace"
           href="${os.path.relpath(os.path.join(dirname, testdelay_subpages[ms]), pcontext.report_dir)}">Test delay plots </a>|
        <a class="replace"
           href="${os.path.relpath(os.path.join(dirname, ampgain_subpages[ms]), pcontext.report_dir)}">Gain Amplitude </a>|
        <a class="replace"
           href="${os.path.relpath(os.path.join(dirname, phasegain_subpages[ms]), pcontext.report_dir)}">Gain Phase </a>|
        <a class="replace"
           href="${os.path.relpath(os.path.join(dirname, bpsolamp_subpages[ms]), pcontext.report_dir)}">BP Amp solution </a>|
        <a class="replace"
           href="${os.path.relpath(os.path.join(dirname, bpsolphase_subpages[ms]), pcontext.report_dir)}">BP Phase solution </a>
    </h4>
%endfor


<%self:plot_group plot_dict="${summary_plots}"
                                  url_fn="${lambda ms:  'noop'}">

        <%def name="title()">
            testBPdcals summary plot
        </%def>

        <%def name="preamble()">


        </%def>
        
        
        <%def name="mouseover(plot)">Summary window </%def>
        
        
        
        <%def name="fancybox_caption(plot)">
          Initial calibrated bandpass
        </%def>
        
        
        <%def name="caption_title(plot)">
           Initial calibrated bandpass
        </%def>
</%self:plot_group>


<h3>Flag bad deformatters</h3>

<p>Identify and flag basebands with bad deformatters or RFI based on bandpass (BP) table amps and phases.</p>


        % for single_result in result:
            <h3>BP Table Amps</h3>
            <table class="table table-bordered table-striped table-condensed"
	       summary="Deformatter Flagging Amp">
	    <caption></caption>
	    <thead>
		<tr>
			<th>Antenna</th>
			<th>SPWs</th>
			<th>Band / Basebands</th>
		</tr>
	    </thead>
	    <tbody>

	    % if single_result.result_amp == []:
	        <tr>
	        <td>None</td>
	        <td>None</td>
	        <td>None</td>
	        </tr>
	    % else:
	        % for key, valueDict in single_result.amp_collection.items():
	            <tr>
	            <td>${key}</td>
	            <td>${','.join(valueDict['spws'])}</td>
	            <td>${','.join(valueDict['basebands'])}</td>
	            </tr>
	        % endfor
	    % endif

	    </tbody>
            </table>
            <br>

            <h3>BP Table Phases</h3>
            <table class="table table-bordered table-striped table-condensed"
	       summary="Deformatter Flagging Phase">
	    <caption></caption>
	    <thead>
		<tr>
			<th>Antenna</th>
			<th>SPWs</th>
			<th>Band / Basebands</th>
		</tr>
	    </thead>
	    <tbody>

	    % if single_result.result_phase == []:
	        <tr>
	        <td>None</td>
	        <td>None</td>
	        <td>None</td>
	        </tr>
	    % else:
	        % for key, valueDict in single_result.phase_collection.items():
	            <tr>
	            <td>${key}</td>
	            <td>${','.join(valueDict['spws'])}</td>
	            <td>${','.join(valueDict['basebands'])}</td>
	            </tr>
	        % endfor
	    % endif

	    </tbody>
            </table>

        % endfor





