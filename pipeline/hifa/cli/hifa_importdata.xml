<casaxml xsi:schemaLocation="http://casa.nrao.edu/schema/casa.xsd file:///opt/casa/code/xmlcasa/xml/casa.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://casa.nrao.edu/schema/psetTypes.html">

    <task category="pipeline" name="hifa_importdata" type="function">

        <shortdescription>Imports data into the interferometry pipeline</shortdescription>
        <description>Imports data into the interferometry pipeline.</description>

        <input>
            <param name="vis" type="stringVec">
                <shortdescription>List of input visibility data</shortdescription>
                <description>List of visibility data files.
                    These may be ASDMs, tar files of ASDMs,
                    MSes, or tar files of MSes. If ASDM files are specified, they will be
                    converted to MS format.
                    example: vis=['X227.ms', 'asdms.tar.gz']
                </description>
                <value/>
            </param>

            <param name="session" type="stringVec">
                <description>List of visibility data sessions</description>
                <value/>
            </param>

            <param name="pipelinemode" type="string">
                <shortdescription>The pipeline operating mode</shortdescription>
                <description>The pipeline operating mode.
                    In 'automatic' mode the pipeline determines the values of all
                    context defined pipeline inputs automatically.
                    In 'interactive' mode the user can set the pipeline context defined
                    parameters manually. In 'getinputs' mode the user can check the settings of
                    all pipeline parameters without running the task.
                </description>
                <value>automatic</value>
                <allowed kind="enum">
                    <value>automatic</value>
                    <value>interactive</value>
                    <value>getinputs</value>
                </allowed>
            </param>

            <param subparam="true" name="asis" type="string">
                <description>Extra ASDM tables to convert as is</description>
                <value>SBSummary ExecBlock Antenna Station Receiver Source CalAtmosphere CalWVR CalPointing</value>
            </param>

            <param subparam="true" name="process_caldevice" type="bool">
                <description>Import the caldevice table from the ASDM</description>
                <value>False</value>
            </param>

            <param subparam="true" name="overwrite" type="bool">
                <description>Overwrite existing files on import</description>
                <value>False</value>
            </param>

            <param subparam="true" name="nocopy" type="bool">
                <description>Disable copying of MS to working directory</description>
                <value>False</value>
            </param>

            <param subparam="true" name="bdfflags" type="bool">
                <description>Apply BDF flags on import</description>
                <value>True</value>
            </param>

            <param name="asimaging" type="bool">
                <description>Import MeasurementSets as imaging MeasurementSets</description>
                <value>False</value>
            </param>

            <param name="lazy" type="bool">
                <description>Use the lazy filler import</description>
                <value>False</value>
            </param>

            <param name="dbservice" type="bool">
                <description>Use the online flux catalog</description>
                <value>False</value>
            </param>

            <param subparam="true" name="ocorr_mode" type="string">
                <description>ALMA default set to ca</description>
                <value>ca</value>
            </param>

            <param name="createmms" type="string">
                <description>Create an MMS</description>
                <value>false</value>
                <allowed kind="enum">
                    <value>false</value>
                    <value>true</value>
                    <value>automatic</value>
                </allowed>
            </param>

            <param name="minparang" type="double">
                <shortdescription>Parallactic angle threshold</shortdescription>
                <description>Minimum required parallactic angle range for polarisation calibrator,
                             in degrees. The default of 0.0 is used for non-polarisation processing.</description>
                <value>0.0</value>
            </param>

            <param subparam="true" name="dryrun" type="bool">
                <description>Run the task (False) or display task command (True)</description>
                <value>False</value>
            </param>

            <param subparam="true" name="acceptresults" type="bool">
                <description>Add the results into the pipeline context</description>
                <value>True</value>
            </param>

            <constraints>
                <when param="pipelinemode">
                    <equals value="automatic" type="string">
                    </equals>
                    <equals value="interactive" type="string">
                        <default param="asis">
                            <value type="string">SBSummary ExecBlock Antenna Station Receiver Source CalAtmosphere CalWVR CalPointing</value>
                        </default>
                        <default param="ocorr_mode">
                            <value type="string">ca</value>
                        </default>
                        <default param="process_caldevice">
                            <value type="bool">False</value>
                        </default>
                        <default param="overwrite">
                            <value type="bool">False</value>
                        </default>
                        <default param="nocopy">
                            <value type="bool">False</value>
                        </default>
                        <default param="bdfflags">
                            <value type="bool">True</value>
                        </default>
                        <default param="dryrun">
                            <value type="bool">False</value>
                        </default>
                        <default param="acceptresults">
                            <value type="bool">True</value>
                        </default>
                    </equals>
                    <equals value="getinputs" type="string">
                        <default param="asis">
                            <value type="string">SBSummary ExecBlock Antenna Station Receiver Source CalAtmosphere CalWVR</value>
                        </default>
                        <default param="process_caldevice">
                            <value type="bool">False</value>
                        </default>
                        <default param="overwrite">
                            <value type="bool">False</value>
                        </default>
                        <default param="nocopy">
                            <value type="bool">False</value>
                        </default>
                        <default param="bdfflags">
                            <value type="bool">True</value>
                        </default>
                        <default param="ocorr_mode">
                            <value type="string">ca</value>
                        </default>
                    </equals>
                </when>
            </constraints>

        </input>

        
        <returns>any</returns>

        <example>
The hifa_importdata task loads the specified visibility data into the pipeline
context unpacking and / or converting it as necessary.


Keyword arguments

---- pipeline parameter arguments which can be set in any pipeline mode


session -- List of sessions to which the visibility files belong. Defaults
    to a single session containing all the visibility files, otherwise
    a session must be assigned to each vis file.

    default: []
    example: session=['session_1', 'session_2']



---- pipeline context defined parameter argument which can be set only in
'interactive mode'

asis -- ASDM tables to convert as is.

    default: 'SBSummary ExecBlock Antenna Station Receiver Source CalAtmosphere CalWVR'
    example: asis='Receiver', asis=''

process_caldevice -- Ingest the ASDM caldevice table.

    default: False
    example: process_caldevice=True

overwrite -- Overwrite existing MSes on output.

    default: False

nocopy -- When importing an MS, disable copying of the MS to the working
    directory.

    default: False

bdfflags -- Apply BDF flags on line.

    default: True

ocorr_mode -- Read in cross- and auto-correlation data(ca), cross-
    correlation data only (co), or autocorrelation data only (ao).

    default: ca

lazy -- Use the lazy filter import.

    default: False

dbservice -- Use online flux catalog on import.

    default: False

ocorr_mode -- Read in cross- and auto-correlation data(ca), cross-
    correlation data only (co), or autocorrelation data only (ao).

    default: ca

createmms -- Create a multi-MeasurementSet ('true') ready for full parallel 
    processing, or a standard MeasurementSet ('false'). The default setting
    ('automatic') creates an MMS if running in a cluster environment.

    default: 'false'

--- pipeline task execution modes

dryrun -- Run the commands (True) or generate the commands to be run but
    do not execute (False).

    default: True

acceptresults -- Add the results of the task to the pipeline context (True) or
    reject them (False).

    default: True


Output

results -- If pipeline mode is 'getinputs' then None is returned. Otherwise
    the results object for the pipeline task is returned.


Examples

1. Load an ASDM list in the ../rawdata subdirectory into the context:

    hifa_importdata(vis=['../rawdata/uid___A002_X30a93d_X43e',
                         '../rawdata/uid_A002_x30a93d_X44e'])

2. Load an MS in the current directory into the context:

    hifa_importdata(vis=[uid___A002_X30a93d_X43e.ms])

3. Load a tarred ASDM in ../rawdata into the context:

    hifa_importdata(vis=['../rawdata/uid___A002_X30a93d_X43e.tar.gz'])

4. Check the hif_importdata inputs, then import the data:

    myvislist = ['uid___A002_X30a93d_X43e.ms', 'uid_A002_x30a93d_X44e.ms']
    hifa_importdata(vis=myvislist, pipelinemode='getinputs')
    hifa_importdata(vis=myvislist)

5. Load an ASDM but check the results before accepting them into the context.

    results = hifa_importdata(vis=['uid___A002_X30a93d_X43e.ms'],
                              acceptresults=False)
    results.accept()

6. Run in dryrun mode before running for real:

    results = hifa_importdata(vis=['uid___A002_X30a93d_X43e.ms'], dryrun=True)
    results = hifa_importdata(vis=['uid___A002_X30a93d_X43e.ms'])
        </example>
    </task>
</casaxml>
