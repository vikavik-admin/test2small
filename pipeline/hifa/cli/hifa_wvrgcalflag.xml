<casaxml xsi:schemaLocation="http://casa.nrao.edu/schema/casa.xsd file:///opt/casa/code/xmlcasa/xml/casa.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://casa.nrao.edu/schema/psetTypes.html">

<task category="pipeline" name="hifa_wvrgcalflag" type="function">

<shortdescription>
    Generate a gain table based on Water Vapor Radiometer data, interpolating over
    antennas with bad radiometers.
</shortdescription>
<description>
    This task will first identify for each vis whether it includes at least 3
    antennas with Water Vapor Radiometer (WVR) data, and that the fraction of
    WVR antennas / all antennas exceeds the minimum threshold
    (ants_with_wvr_thresh).

    If there are not enough WVR antennas by number and/or fraction, then no WVR
    caltable is created and no WVR calibration will be applied to the corresponding
    vis. If there are enough WVR antennas, then the task proceeds as follows for
    each valid vis:

    First, generate a gain table based on the Water Vapor Radiometer data for
    each vis.

    Second, apply the WVR calibration to the data specified by 'flag_intent',
    calculate flagging 'views' showing the ratio
    'phase-rms with WVR / phase-rms without WVR' for each scan. A ratio &lt; 1
    implies that the phase noise is improved, a score &gt; 1 implies that it
    is made worse.

    Third, search the flagging views for antennas with anomalous high values.
    If any are found then recalculate the WVR calibration with the 'wvrflag'
    parameter set to ignore their data and interpolate results from other
    antennas according to 'maxdistm' and 'minnumants'.

    Fourth, after flagging, if the remaining unflagged antennas with WVR number
    fewer than 3, or represent a smaller fraction of antennas than the minimum
    threshold (ants_with_wvr_thresh), then the WVR calibration file is rejected
    and will not be merged into the context, i.e. not be used in subsequent
    calibration.

    Fifth, if the overall QA score for the final WVR correction of a vis file
    is greater than the value in 'accept_threshold' then make available the
    wvr calibration file for merging into the context and use in the
    subsequent reduction.
</description>

<input>
    <param subparam="true" name="vis" type="stringVec">
        <shortdescription>List of input visibility files</shortdescription>
        <description>List of input visibility files.

            default: none, in which case the vis files to be used will be read
                     from the context
            example: vis=['ngc5921.ms']
        </description>
        <value/>
    </param>

    <param subparam="true" name="caltable" type="stringVec">
        <shortdescription>List of output gain calibration tables</shortdescription>
        <description>List of output gain calibration tables.

            default: none, in which case the names of the caltables will be
                     generated automatically
            example: caltable='ngc5921.wvr'
        </description>
        <value/>
    </param>

    <param subparam="true" name="offsetstable" type="stringVec">
        <shortdescription>List of input temperature offsets table files</shortdescription>
        <description>List of input temperature offsets table files to subtract from
            WVR measurements before calculating phase corrections.

            default: none, in which case no offsets are applied
            example: offsetstable=['ngc5921.cloud_offsets']
        </description>
        <value/>
    </param>

    <param name="hm_toffset" type="string">
        <shortdescription>Toffset computation heuristic method</shortdescription>
        <description>If 'manual', set the 'toffset' parameter to the user-specified
            value. If 'automatic', set the 'toffset' parameter according to the
            date of the MeasurementSet; toffset=-1 if before 2013-01-21T00:00:00
            toffset=0 otherwise.
        </description>
        <value>automatic</value>
        <allowed kind="enum">
            <value>manual</value>
            <value>automatic</value>
        </allowed>
    </param>

    <param subparam="true" name="toffset" type="double">
        <shortdescription>Time offset (sec) between IF and WVR data</shortdescription>
        <description>Time offset (sec) between interferometric and WVR data.</description>
        <value>0</value>
    </param>

    <param subparam="true" name="segsource" type="bool">
        <shortdescription>Compute new coefficient calculation for each source</shortdescription>
        <description>If True calculate new atmospheric phase correction
            coefficients for each source, subject to the constraints of
            the 'tie' parameter. 'segsource' is forced to be True if
            the 'tie' parameter is set to a non-empty value by the
            user or by the automatic heuristic.
        </description>
        <value>True</value>
    </param>

    <param subparam="true" name="sourceflag" type="stringVec">
        <shortdescription>Flag the WVR data for these source(s)</shortdescription>
        <description>Flag the WVR data for these source(s) as bad and do not produce
            corrections for it. Requires segsource=True.

            example: sourceflag=['3C273']
        </description>
        <value/>
    </param>

    <param name="hm_tie" type="string">
        <shortdescription>Tie computation heuristics method</shortdescription>
        <description>If 'manual', set the 'tie' parameter to the user-specified value.
            If 'automatic', set the 'tie' parameter to include with the
            target all calibrators that are within 15 degrees of it:
            if no calibrators are that close then 'tie' is left empty.
        </description>
        <value>automatic</value>
        <allowed kind="enum">
            <value>automatic</value>
            <value>manual</value>
        </allowed>
    </param>

    <param subparam="true" name="tie" type="stringVec">
        <shortdescription>Sources for which to use the same atmospheric phase correction coefficients</shortdescription>
        <description>Use the same atmospheric phase correction coefficients when
            calculating the WVR correction for all sources in the 'tie'. If 'tie'
            is not empty then 'segsource' is forced to be True. Ignored unless
            hm_tie='manual'.

            example: tie=['3C273,NGC253', 'IC433,3C279']
        </description>
        <value/>
    </param>

    <param subparam="true" name="nsol" type="int">
        <shortdescription>Number of solutions for phase correction coefficients</shortdescription>
        <description>Number of solutions for phase correction coefficients during this
            observation, evenly distributed in time throughout the observation. It
            is used only if segsource=False because if segsource=True then the
            coefficients are recomputed whenever the telescope moves to a new
            source (within the limits imposed by 'tie').
        </description>
        <value>1</value>
    </param>

    <param name="disperse" type="bool">
        <shortdescription>Apply correction for dispersion</shortdescription>
        <description>Apply correction for dispersion.</description>
        <value>False</value>
    </param>

    <param subparam="true" name="wvrflag" type="stringVec">
        <shortdescription>Flag the WVR data for these antenna(s) replace with interpolated values</shortdescription>
        <description>Flag the WVR data for these antenna(s) as bad and replace its data
            with interpolated values.

            example: wvrflag=['DV03','DA05','PM02']
        </description>
        <value/>
    </param>

    <param name="hm_smooth" type="string">
        <shortdescription>Smoothing computation heuristics method</shortdescription>
        <description>If 'manual' set the 'smooth' parameter to the user-specified value.
            If 'automatic', run the wvrgcal task with the range of 'smooth' parameters
            required to match the integration time of the WVR data to that of the
            interferometric data in each spectral window.
        </description>
        <value>automatic</value>
        <allowed kind="enum">
            <value>automatic</value>
            <value>manual</value>
        </allowed>
    </param>

    <param subparam="true" name="smooth" type="string">
        <shortdescription>Smooth WVR data on the given timescale before calculating the correction</shortdescription>
        <description>Smooth WVR data on this timescale before calculating the correction.
            Ignored unless hm_smooth='manual'.
        </description>
        <value/>
    </param>

    <param subparam="true" name="scale" type="double">
        <shortdescription>Scale the entire phase correction by this factor</shortdescription>
        <description>Scale the entire phase correction by this factor.</description>
        <value>1</value>
    </param>

    <param name="maxdistm" type="double">
        <shortdescription>Maximum distance (m) of an antenna used for interpolation for a flagged antenna</shortdescription>
        <description> Maximum distance in meters of an antenna used for interpolation
            from a flagged antenna.

            default: -1  (automatically set to 100m if &gt;50% of antennas are 7m
                     antennas without WVR and otherwise set to 500m)
            example: maxdistm=550
        </description>
        <value>-1</value>
    </param>

    <param name="minnumants" type="int">
        <shortdescription>Minimum number of near antennas (up to 3) required for interpolation</shortdescription>
        <description>Minimum number of nearby antennas (up to 3) used for
            interpolation from a flagged antenna.

            example: minnumants=3
        </description>
        <value>2</value>
        <allowed kind="enum">
            <value>1</value>
            <value>2</value>
            <value>3</value>
        </allowed>
    </param>

    <param name="mingoodfrac" type="double">
        <shortdescription>Minimum fraction of good data per antenna</shortdescription>
        <description>Minimum fraction of good data per antenna.

            example: mingoodfrac=0.7
        </description>
        <value>0.8</value>
    </param>

    <param subparam="true" name="refant" type="string">
      <shortdescription>Ranked list of reference antennas</shortdescription>
      <description>Ranked comma delimited list of reference antennas.

          example: refant='DV02,DV06'
      </description>
      <value/>
    </param>

    <param name="flag_intent" type="string">
        <shortdescription>Data intents to use in detecting and flagging bad WVR antennas</shortdescription>
        <description>The data intent(s) on whose WVR correction results the search
            for bad WVR antennas is to be based.

            A 'flagging view' will be calculated for each specified intent, in each
            spectral window in each vis file.

            Each 'flagging view' will consist of a 2-d image with dimensions
            ['ANTENNA', 'TIME'], showing the phase noise after the WVR
            correction has been applied.

            If flag_intent is left blank, the default, the flagging views will be
            derived from data with the default bandpass calibration intent i.e.
            the first in the list BANDPASS, PHASE, AMPLITUDE for which the
            MeasurementSet has data.
        </description>
        <value/>
    </param>

    <param name="qa_intent" type="string">
        <shortdescription>Data intents to use in estimating the effectiveness of the WVR correction</shortdescription>
        <description>The list of data intents on which the WVR correction is to be
            tried as a means of estimating its effectiveness.

            A QA 'view' will be calculated for each specified intent, in each spectral
            window in each vis file.

            Each QA 'view' will consist of a pair of 2-d images with dimensions
            ['ANTENNA', 'TIME'], one showing the data phase-noise before the
            WVR application, the second showing the phase noise after (both 'before'
            and 'after' images have a bandpass calibration applied as well).

            An overall QA score is calculated for each vis file, by dividing the
            'before' images by the 'after' and taking the median of the result. An
            overall score of 1 would correspond to no change in the phase noise,
            a score &gt; 1 implies an improvement.

            If the overall score for a vis file is less than the value in
            'accept_threshold' then the WVR calibration file is not made available for
            merging into the context for use in the subsequent reduction.
        </description>
        <value>BANDPASS,PHASE</value>
    </param>

    <param name="qa_bandpass_intent" type="string">
        <shortdescription>Data intent to use for the bandpass calibration in the qa calculation</shortdescription>
        <description>The data intent to use for the bandpass calibration in
            the qa calculation. The default is blank to allow the underlying bandpass
            task to select a sensible intent if the dataset lacks BANDPASS data.
        </description>
        <value/>
    </param>

    <param name="accept_threshold" type="double">
        <shortdescription>Improvement ratio (phase-rms without WVR / phase-rms with WVR) above which wvrg calibration file will be accepted</shortdescription>
        <description>The phase-rms improvement ratio
            (rms without WVR / rms with WVR) above which the wrvg file will be
            accepted into the context for subsequent application.
        </description>
        <value>1.0</value>
    </param>

    <param name="flag_hi" type="bool">
        <shortdescription>True to flag high figure of merit outliers</shortdescription>
        <description>True to flag high figure of merit outliers.</description>
        <value>True</value>
    </param>

    <param subparam="true" name="fhi_limit" type="double">
        <shortdescription>Flag figure of merit values higher than limit * MAD</shortdescription>
        <description>Flag figure of merit values higher than limit * MAD.</description>
        <value>10.0</value>
    </param>

    <param subparam="true" name="fhi_minsample" type="int">
        <shortdescription>Minimum number of samples for valid MAD estimate</shortdescription>
        <description>Minimum number of samples for valid MAD estimate/</description>
        <value>5</value>
    </param>

    <param name="ants_with_wvr_thresh" type="double">
        <shortdescription>Minimum fraction of unflagged antennas that need to have WVR for calibration to proceed.</shortdescription>
        <description>this threshold sets the minimum fraction of antennas
            that should have WVR data for WVR calibration and flagging to proceed; the
            same threshold is used to determine, after flagging, whether there remain
            enough unflagged antennas with WVR data for the WVR calibration to be
            applied.

            example: ants_with_wvr_thresh=0.5
        </description>
        <value>0.2</value>
    </param>

    <param name="pipelinemode" type="string">
        <description>The pipeline operating mode</description>
        <value>automatic</value>
        <allowed kind="enum">
            <value>automatic</value>
            <value>interactive</value>
            <value>getinputs</value>
        </allowed>
    </param>

    <param subparam="true" name="dryrun" type="bool">
        <description>Run the task (False) or display the command(True)</description>
        <value>False</value>
    </param>

    <param subparam="true" name="acceptresults" type="bool">
       <description>Add the results to the pipeline context</description>
       <value>True</value>
    </param>

    <constraints>

        <when param="pipelinemode">
            <equals value="automatic" type="string"/>
            <equals value="interactive" type="string">
                <default param="vis"><value type="stringVec"/></default>
                <default param="caltable"><value type="stringVec"/></default>
                <default param="offsetstable"><value type="stringVec"/></default>
                <default param="wvrflag"><value type="stringVec"/></default>
                <default param="scale"><value type="double">1.0</value></default>
                <default param="refant"><value type="string"/></default>
                <default param="dryrun"><value type="bool">False</value></default>
                <default param="acceptresults"><value type="bool">True</value></default>
            </equals>
            <equals value="getinputs" type="string">
                <default param="vis"><value type="stringVec"/></default>
                <default param="caltable"><value type="stringVec"/></default>
                <default param="offsetstable"><value type="stringVec"/></default>
                <default param="wvrflag"><value type="stringVec"/></default>
                <default param="scale"><value type="double">1.0</value></default>
                <default param="refant"><value type="string"/></default>
            </equals>
        </when>

        <when param="hm_smooth">

      <equals value="automatic" type="string">
      </equals>
      <equals value="manual" type="string">
          <default param="smooth"><value type="string"/></default>
      </equals>
        </when>

    <when param="hm_tie">
      <equals value="automatic" type="string">
      </equals>
      <equals value="manual" type="string">
          <default param="sourceflag"><value type="stringVec"/></default>
          <default param="tie"><value type="stringVec"/></default>
          <default param="segsource"><value type="bool">False</value></default>
                <default param="nsol"><value type="int">1</value></default>
      </equals>
    </when>

        <when param="hm_toffset">
            <equals value="automatic" type="string">
            </equals>
            <equals value="manual" type="string">
                <default param="toffset"><value type="double">0</value></default>
            </equals>
        </when>

        <when param="flag_hi">
            <equals value="True" type="bool">
                <default param="fhi_limit"><value>10.0</value></default>
                <default param="fhi_minsample"><value>5</value></default>
            </equals>
            <equals value="False" type="bool">
            </equals>
        </when>

    </constraints>

</input>


<returns>any</returns>

<example>
1. Compute the WVR calibration for all the MeasurementSets:

    hifa_wvrgcalflag(hm_tie='automatic')
</example>
</task>
</casaxml>
