<casaxml xsi:schemaLocation="http://casa.nrao.edu/schema/casa.xsd file:///opt/casa/code/xmlcasa/xml/casa.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://casa.nrao.edu/schema/psetTypes.html">

<task category="pipeline" name="hsdn_exportdata" type="function">
<shortdescription>Prepare single dish data for export</shortdescription>
<description>
The hsdn_exportdata task exports the data defined in the pipeline context
and exports it to the data products directory, converting and or packing
it as necessary.

The current version of the task exports the following products

o a FITS image for each selected science target source image
o a tar file per ASDM containing the final flags version and blparam
o a tar file containing the file web log

TBD
o a file containing the line feature table(frequency, width, spatial distribution)
o a file containing the list of identified transitions from line catalogs

Output:

results -- If pipeline mode is 'getinputs' then None is returned. Otherwise
   the results object for the pipeline task is returned.
</description>
<input>
    <param subparam="true" name="pprfile" type="string">
        <shortdescription>The pipeline processing request (PPR) file to be exported</shortdescription>
        <description>Name of the pipeline processing request to be exported. Defaults
        to a file matching the template 'PPR_*.xml'.
        Parameter is not available when pipelinemode='automatic'.
        example: pprfile=['PPR_GRB021004.xml']</description>
        <value/>
    </param>
    
    <param subparam="true" name="targetimages" type="stringVec">
        <shortdescription>List of target CASA images to be exported</shortdescription>
        <description>List of science target images to be exported. Defaults to all
        science target images recorded in the pipeline context.
        Parameter is not available when pipelinemode='automatic'.
        example: targetimages=['r_aqr.CM02.spw5.line0.XXYY.sd.im', 'r_aqr.CM02.spw5.XXYY.sd.cont.im']</description>
        <value/>
    </param>
    
    <param subparam="true" name="products_dir" type="string">
        <shortdescription>The data products directory</shortdescription>
        <description>Name of the data products subdirectory. Defaults to './'
        Parameter is not available when pipelinemode='automatic'.
        example: products_dir='../products'</description>
        <value/>
    </param>

    <param name="pipelinemode" type="string">
	<shortdescription>The pipeline operating mode</shortdescription>
        <description>The pipeline operating mode. In 'automatic' mode the pipeline
        determines the values of all context defined pipeline inputs automatically.
        In 'interactive' mode the user can set the pipeline context defined
        parameters manually.  In 'getinputs' mode the user can check the settings
        of all pipeline parameters without running the task.</description>
	<value>automatic</value>
	<allowed kind="enum">
	    <value>automatic</value>
	    <value>interactive</value>
	    <value>getinputs</value>
	</allowed>
    </param>

    <param subparam="true" name="dryrun" type="bool">
	<shortdescription>Run the task (False) or display task command (True)</shortdescription>
        <description>Run the task (False) or display task command (True).
        Only available when pipelinemode='interactive'.</description>
	<value>False</value>
    </param>

    <param subparam="true" name="acceptresults" type="bool">
	<shortdescription>Add the results into the pipeline context</shortdescription>
        <description>Add the results of the task to the pipeline context (True) or
        reject them (False). Only available when pipelinemode='interactive'.</description>
	<value>True</value>
    </param>

    <constraints>
	<when param="pipelinemode">
	    <equals value="automatic" type="string">
	    </equals>
	    <equals value="interactive" type="string">
		<default param="pprfile"><value type="string"/></default>
		<default param="targetimages"><value type="stringVec"/></default>
        <default param="products_dir"><value type="string"/></default>
		<default param="dryrun"><value type="bool">False</value></default>
		<default param="acceptresults"><value type="bool">True</value></default>
	    </equals>
	    <equals value="getinputs" type="string">
		<default param="pprfile"><value type="string"/></default>
		<default param="targetimages"><value type="stringVec"/></default>
        <default param="products_dir"><value type="string"/></default>
	    </equals>
	</when>
    </constraints>

</input>


<returns>any</returns>


<example>
1. Export the pipeline results for a single session to the data products
directory

    !mkdir ../products
    hsdn_exportdata (products_dir='../products')

</example>
</task>
</casaxml>
