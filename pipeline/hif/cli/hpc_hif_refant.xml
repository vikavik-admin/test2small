<casaxml xsi:schemaLocation="http://casa.nrao.edu/schema/casa.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://casa.nrao.edu/schema/psetTypes.html">

    <task category="pipeline" name="hpc_hif_refant" type="function">

        <shortdescription>Select the best reference antennas</shortdescription>
        <description>
The hpc_hif_refant task selects a list of reference antennas and stores them
in the pipeline context in priority order.

The priority order is determined by a weighted combination of scores derived
by the antenna selection heuristics. In manual mode the reference antennas
can be set by hand.


Output:

results -- If pipeline mode is 'getinputs' then None is returned. Otherwise
the results object for the pipeline task is returned.

Issues

        </description>

        <input>
            <param subparam="true" name="vis" type="stringVec">
                <shortdescription>List of input MeasurementSets</shortdescription>
                <description>The list of input MeasurementSets. Defaults to the list of
                MeasurementSets in the pipeline context.
                Not available when pipelinemode='automatic'.
                example: ['M31.ms']</description>
                <value/>
            </param>

            <param subparam="true" name="field" type="string">
                <shortdescription>List of field names or ids</shortdescription>
                <description>The comma delimited list of field names or field ids for which flagging
                scores are computed if hm_refant='automatic' and flagging = True.
                Not available when pipelinemode='automatic'.
                example: '' (Default to fields with the specified intents), '3C279', '3C279,M82'</description>
                <value/>
            </param>

            <param subparam="true" name="intent" type="string">
                <shortdescription>List of data selection intents</shortdescription>
                <description>A string containing a comma delimited list of intents against
                which the selected fields are matched. Defaults to all supported
                intents. Not available when pipelinemode='automatic'.
                example: 'BANDPASS', 'AMPLI,BANDPASS,PHASE'</description>
                <value>AMPLITUDE,BANDPASS,PHASE</value>
            </param>

            <param subparam="true" name="spw" type="string">
                <shortdescription>List of spectral windows ids</shortdescription>
                <description>A string containing the comma delimited list of spectral window ids for
                which flagging scores are computed if hm_refant='automatic' and flagging = True.
                Not available when pipelinemode='automatic'.
                example: '' (all spws observed with the specified intents), '11,13,15,17'</description>
                <value/>
            </param>

            <param name="hm_refant" type="string">
                <shortdescription>The reference antenna heuristics mode</shortdescription>
                <description>The heuristics method or mode for selection the reference
                antenna. The options are 'manual' and 'automatic. In manual
                mode a user supplied referenence antenna refant is supplied.
                In 'automatic' mode the antennas are selected automatically.</description>
                <value>automatic</value>
                <allowed kind="enum">
                    <value>automatic</value>
                    <value>manual</value>
                </allowed>
            </param>

            <param subparam="true" name="refant" type="string">
                <shortdescription>List of reference antennas</shortdescription>
                <description>The user supplied reference antenna for hm_refant='manual. If
                no antenna list is supplied an empty list is returned.
                example: 'DV05'</description>
                <value/>
            </param>

            <param subparam="true" name="refantignore" type="string">
                <shortdescription>String list of antennas to ignore</shortdescription>
                <description>string list to be ignored as reference antennas.
                example: refantignore='ea02,ea03'</description>
                <value/>
            </param>

            <param subparam="true" name="geometry" type="bool">
                <shortdescription>Score by proximity to center of the array</shortdescription>
                <description>Score antenna by proximity to the center of the array. This
                option is quick as only the ANTENNA table must be read.
                Parameter is available when hm_refant='automatic'.</description>
                <value>True</value>
            </param>

            <param subparam="true" name="flagging" type="bool">
                <shortdescription>Score by percentage of good data</shortdescription>
                <description>Score antennas by percentage of unflagged data. This option
                requires computing flagging statistics.
                Parameter is available when hm_refant='automatic'.</description>
                <value>True</value>
            </param>

            <param name="pipelinemode" type="string">
                <shortdescription>The pipeline operating mode</shortdescription>
                <description>The pipeline operating mode. In 'automatic' mode the pipeline
                determines the values of all context defined pipeline inputs automatically.
                In interactive mode the user can set the pipeline context defined parameters
                manually. In 'getinputs' mode the user can check the settings of all
                pipeline parameters without running the task.</description>
                <value>automatic</value>
                <allowed kind="enum">
                    <value>automatic</value>
                    <value>interactive</value>
                    <value>getinputs</value>
                </allowed>
            </param>

            <param subparam="true" name="dryrun" type="bool">
                <shortdescription>Run the task (False) or display the command (True)</shortdescription>
                <description>Run the task (False) or display the command (True).
                Available when pipelinemode='interactive'.</description>
                <value>False</value>
            </param>

            <param subparam="true" name="acceptresults" type="bool">
                <shortdescription>Add the results into the pipeline context</shortdescription>
                <description>Add the results of the task to the pipeline context (True) or
                reject them (False). Available when pipelinemode='interactive'.</description>
                <value>True</value>
            </param>

            <param subparam="true" name="parallel" type="string">
                <description>Execute using CASA HPC functionality, if available.</description>
                <value>automatic</value>
                <allowed kind="enum">
                    <value>automatic</value>
                    <value>true</value>
                    <value>false</value>
                </allowed>
            </param>

            <constraints>
                <when param="pipelinemode">
                    <equals value="automatic" type="string">
                    </equals>
                    <equals value="interactive" type="string">
                        <default param="vis">
                            <value type="stringVec"/>
                        </default>
                        <default param="field">
                            <value type="string"/>
                        </default>
                        <default param="intents">
                            <value type="string">AMPLITUDE,BANDPASS,PHASE</value>
                        </default>
                        <default param="spw">
                            <value type="string"/>
                        </default>
                        <default param="dryrun">
                            <value type="bool">False</value>
                        </default>
                        <default param="acceptresults">
                            <value type="bool">True</value>
                        </default>
                        <default param="parallel">
                            <value type="string">automatic</value>
                        </default>
                    </equals>
                    <equals value="getinputs" type="string">
                        <default param="vis">
                            <value type="stringVec"/>
                        </default>
                        <default param="field">
                            <value type="string"/>
                        </default>
                        <default param="intents">
                            <value type="string">AMPLITUDE,BANDPASS,PHASE</value>
                        </default>
                        <default param="spw">
                            <value type="string"/>
                        </default>
                    </equals>
                </when>

                <when param="hm_refant">
                    <equals value="automatic" type="string">
                        <default param="geometry">
                            <value type="bool">True</value>
                        </default>
                        <default param="flagging">
                            <value type="bool">True</value>
                        </default>
                    </equals>
                    <equals value="manual" type="string">
                        <default param="refant">
                            <value type="string"/>
                        </default>
                    </equals>
                </when>
            </constraints>
        </input>

        
        <returns>any</returns>

        <example>

1. Compute the references antennas to be used for bandpass and gain calibration.

hpc_hif_refant()
        </example>
    </task>
</casaxml>
