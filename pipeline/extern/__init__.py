import pkg_resources
import sys

sys.path.append(pkg_resources.resource_filename(__name__, 'Mako-1.1.0-py3.6.egg'))
import mako

sys.path.append(pkg_resources.resource_filename(__name__, 'cachetools-3.1.1-py3.6.egg'))
import cachetools

sys.path.append(pkg_resources.resource_filename(__name__, 'sortedcontainers-1.4.4-py3.6.egg'))
import sortedcontainers

sys.path.append(pkg_resources.resource_filename(__name__, 'intervaltree-2.1.0-py3.6.egg'))
import intervaltree

from . import logutils
from . import XmlObjectifier
